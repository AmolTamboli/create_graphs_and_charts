//
//  ViewController.swift
//  Graph
//
//  Created by Amol Tamboli on 30/06/20.
//  Copyright © 2020 Amol_Tamboli. All rights reserved.
//

import UIKit
import Charts

class ViewController: UIViewController,ChartViewDelegate {

    var barChart = BarChartView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.barChart.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        barChart.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.width)
       
        barChart.center = view.center
        
        view.addSubview(barChart)
        
        var entries = [BarChartDataEntry]()
        
        for x in 0..<10 {
            entries.append(BarChartDataEntry(x: Double(x), y: Double(x)))
        }
        
        let set = BarChartDataSet(entries)
        
        set.colors = ChartColorTemplates.joyful()
        
        let data = BarChartData(dataSet: set)
        
        barChart.data = data
    }

}

